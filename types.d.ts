// Type definitions for winbox-sveltekit

export as namespace WinBox;

interface WinBox {
	autosize: boolean;
	background: string;
	body: HTMLElement;
	border: string | number;
	bottom: string | number;
	class: string | string[];
	dom: Node;
	header: string | number;
	height: string | number;
	hidden: boolean;
	html: string;
	icon: string | boolean;
	id: string | number;
	index: number;
	left: string | number;
	max: boolean;
	minheight: string | number;
	minwidth: string | number;
	modal: boolean;
	mount: Node;
	right: string | number;
	root: Node;
	splitscreen: boolean;
	title: string;
	top: string | number;
	url: string;
	width: string | number;
	x: 'right' | 'center' | string | number;
	y: 'bottom' | 'center' | string | number;
	addClass(classname: string): WinBox;
	close(force?: boolean): boolean | void;
	focus(): WinBox;
	fullscreen(state?: boolean): WinBox;
	hide(): WinBox;
	maximize(state?: boolean): WinBox;
	minimize(state?: boolean): WinBox;
	mount(src?: Element): WinBox;
	move(x?: string | number, y?: string | number, skipUpdate?: boolean): WinBox;
	onblur: (this: WinBox) => void;
	onclose: (this: WinBox, force?: boolean) => boolean | void;
	oncreate: (this: WinBox) => void;
	onfocus: (this: WinBox) => void;
	onfullscreen: (this: WinBox) => void;
	onhide: (this: WinBox) => void;
	onmaximize: (this: WinBox, x: number, y: number) => void;
	onminimize: (this: WinBox, x: number, y: number) => void;
	onmove: (this: WinBox, x: number, y: number) => void;
	onresize: (this: WinBox, width: number, height: number) => void;
	onrestore: (this: WinBox) => void;
	onshow: (this: WinBox) => void;
	removeClass(classname: string): WinBox;
	resize(w?: string | number, h?: string | number, skipUpdate?: boolean): WinBox;
	setBackground(background: string): WinBox;
	setTitle(title: string): WinBox;
	setUrl(url: string): WinBox;
	show(): WinBox;
	unmount(dest?: Element): WinBox;
}

declare namespace WinBox {
	interface WinBoxConstructor {
		(title: string, params?: Params): WinBox;
		(params: Params): WinBox;
		new (title: string, params?: Params): WinBox;
		new (params: Params): WinBox;
	}

	interface Params {
		autosize?: boolean | undefined;
		background?: string | undefined;
		border?: string | number | undefined;
		bottom?: string | number | undefined;
		class?: string | string[] | undefined;
		header?: string | number | undefined;
		height?: string | number | undefined;
		hidden?: boolean | undefined;
		html?: string | undefined;
		icon?: string | boolean | undefined;
		id?: string | number | undefined;
		index?: number | undefined;
		left?: string | number | undefined;
		max?: boolean | undefined;
		minheight?: string | number | undefined;
		minwidth?: string | number | undefined;
		modal?: boolean | undefined;
		mount?: Node | undefined;
		right?: string | number | undefined;
		root?: Node | undefined;
		splitscreen?: boolean | undefined;
		title?: string | undefined;
		top?: string | number | undefined;
		url?: string | undefined;
		width?: string | number | undefined;
		x?: 'right' | 'center' | string | number | undefined;
		y?: 'bottom' | 'center' | string | number | undefined;
		onblur?: ((this: WinBox) => void) | undefined;
		onclose?: ((this: WinBox, force?: boolean) => boolean | void) | undefined;
		oncreate?: ((this: WinBox) => void) | undefined;
		onfocus?: ((this: WinBox) => void) | undefined;
		onfullscreen?: ((this: WinBox) => void) | undefined;
		onhide?: ((this: WinBox) => void) | undefined;
		onmaximize?: ((this: WinBox, x: number, y: number) => void) | undefined;
		onminimize?: ((this: WinBox, x: number, y: number) => void) | undefined;
		onmove?: ((this: WinBox, x: number, y: number) => void) | undefined;
		onshow?: ((this: WinBox) => void) | undefined;
		onresize?: ((this: WinBox, width: number, height: number) => void) | undefined;
		onrestore?: ((this: WinBox) => void) | undefined;
	}
}

declare const WinBox: WinBox.WinBoxConstructor & {
	new: ((title: string, params?: WinBox.Params) => WinBox) | ((params: WinBox.Params) => WinBox);
};

export = WinBox;
