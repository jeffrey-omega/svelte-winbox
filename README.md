# svelte-winbox

A wrapper component for using WinBox.js in Svelte.

## Install

```bash
npm i svelte-winbox
```

## Basic Usage

```
<script lang="ts">
	import { WinBox } from 'svelte-winbox';

	let createWindow: () => void;
</script>

<button on:click={createWindow}>Create Window</button>

<WinBox bind:createWindow>
	<span>Sveltekit ❤️ Winbox</span>
	<p>This is Winbox running in Sveltekit!</p>
</WinBox>
```

### Full documentation and demo coming soon!
