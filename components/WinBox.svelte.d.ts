import { SvelteComponentTyped } from "svelte";
declare const __propDef: any;
export type WinBoxProps = typeof __propDef.props;
export type WinBoxEvents = typeof __propDef.events;
export type WinBoxSlots = typeof __propDef.slots;
export default class WinBox extends SvelteComponentTyped<WinBoxProps, WinBoxEvents, WinBoxSlots> {
    get createWindow(): any;
}
export {};
